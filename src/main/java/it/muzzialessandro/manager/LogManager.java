package it.muzzialessandro.manager;

import java.sql.Timestamp;

/**
 * Management class for errors
 */
public class LogManager {

    private static final boolean debug = true;

    /**
     * Prints a message to console
     *
     * @param msg   The message
     */
    public static void i(String msg) {
        LogManager.i(msg, false);
    }

    /**
     * Prints a message to console
     *
     * @param msg   The message
     * @param avoidTimestamp    If True, no timestamp will be added to {@code msg}
     */
    public static void i(String msg, boolean avoidTimestamp) {
        if(avoidTimestamp)
            System.out.println(msg);
        else
            System.out.println(new Timestamp(System.currentTimeMillis()) + "\t" + msg);
    }

    /**
     * Prints a message to console if DEBUG is defined
     *
     * @param msg               The message
     */
    public static void d(String msg) {
        LogManager.d(msg, false);
    }

    /**
     * Prints a message to console if DEBUG is defined
     *
     * @param msg               The message
     * @param avoidTimestamp    If True, no timestamp will be added to {@code msg}
     */
    public static void d(String msg, boolean avoidTimestamp) {
        if(debug) {
            if(avoidTimestamp)
                System.out.println(msg);
            else
                System.out.println(new Timestamp(System.currentTimeMillis()) + "\t" + msg);
        }
    }
}
