package it.muzzialessandro.actor;

import com.opencsv.bean.CsvToBeanBuilder;
import it.muzzialessandro.manager.ErrorManager;
import it.muzzialessandro.manager.LogManager;
import it.muzzialessandro.model.Company;
import it.unipr.sowide.actodes.actor.Behavior;
import it.unipr.sowide.actodes.actor.CaseFactory;
import it.unipr.sowide.actodes.actor.Shutdown;
import it.unipr.sowide.actodes.configuration.Configuration;
import it.unipr.sowide.actodes.controller.SpaceInfo;
import it.unipr.sowide.actodes.examples.messaging.Sender;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * The {@code Receiver} class defines a behavior that waits for a specific
 * number of messages from a {@code Sender} actor and then kills itself.
 *
 * @see Sender
 **/

public final class SlaveScan extends Behavior {

    private static final long serialVersionUID = 1L;

    //region Private Fields

    // Actor ID
    private final int id;
    // Number of slaves
    private final int slaveNumber;
    // Companies
    private List<Company> companies;
    // Configuration
    private HashMap<String, Object> config;

    String dataFolderPath;

    //endregion

    public SlaveScan(final int id, final HashMap<String, Object> config) {
        this.id = id;
        this.config = config;
        this.slaveNumber = (int) config.get("slavesNumber");

        init();
    }

    private void init() {
        try {
            Configuration conf = SpaceInfo.INFO.getConfiguration();

            String fileName = conf.getString("csvFileName");
            this.dataFolderPath = conf.getString("dataFolderPath");

            this.companies = new CsvToBeanBuilder(new FileReader(fileName))
                    .withSkipLines(1)
                    .withType(Company.class)
                    .build()
                    .parse();

            int companiesSize = this.companies.size();
            int companiesAmount = (int) Math.floor((float) companiesSize / this.slaveNumber);
            int companiesStartIdx = companiesAmount * this.id;
            if(this.id == this.slaveNumber - 1)
                companiesAmount = companiesSize - companiesStartIdx;

            LogManager.i("Companies from " + companiesStartIdx + " to " + (companiesStartIdx + companiesAmount));
            this.companies = new ArrayList<>(this.companies.subList(companiesStartIdx, companiesStartIdx + companiesAmount));
            System.out.println("Companies size: " + this.companies.size());
        } catch (FileNotFoundException e) {
            ErrorManager.raiseError(2, "Unable to find the US market CSV.", e);
        }
    }

    /**
     * {@inheritDoc}
     **/
    @Override
    public void cases(final CaseFactory c) {

        c.define(START, (m) -> {
            LogManager.i("Slave started");
            LogManager.d(Paths.get("").toAbsolutePath().toString());
            return null;
        });

        c.define(MessagePatterns.START_RETRIEVING_PRICES, (m) -> {

            int i = 0;
            int startYear = (int) this.config.get("startYear");
            int endYear = (int) this.config.get("endYear");
            String dataFrequency = (String) this.config.get("dataFrequency");
            for(Company company : this.companies) {

                try {
                    company.importPrices(this.dataFolderPath, startYear, endYear, dataFrequency);
                } catch (IOException | ParseException e) {
                    LogManager.i("Error retrieving company prices of " + company.getSymbol() + ": " + e.getMessage());
                }

                if(i > 0 && i % 280 == 0) {
                    LogManager.i("Pause 70s for 300 companies asked");
                    try {
                        // sleep for 70s to avoid Yahoo blocks
                        Thread.sleep(70*1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                i++;
            }

            send(m, this.companies);

            return Shutdown.SHUTDOWN;
        });
    }
}
