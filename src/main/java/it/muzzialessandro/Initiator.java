package it.muzzialessandro;

import it.muzzialessandro.manager.ErrorManager;
import it.unipr.sowide.actodes.actor.Behavior;
import it.unipr.sowide.actodes.actor.CaseFactory;
import it.unipr.sowide.actodes.configuration.Configuration;
import it.unipr.sowide.actodes.controller.SpaceInfo;
import it.unipr.sowide.actodes.distribution.activemq.ActiveMqConnector;
import it.unipr.sowide.actodes.executor.active.ThreadCoordinator;
import it.unipr.sowide.actodes.service.creation.Creator;
import it.unipr.sowide.actodes.service.logging.ConsoleWriter;
import it.unipr.sowide.actodes.service.logging.Logger;
import it.unipr.sowide.actodes.service.logging.util.NoCycleProcessing;


/**
 *
 * The {@code Initiator} class defines a behavior that creates
 * a buffer actor and a set of {@code Producer} and {@code Consumer} actors.
 *
 * After a fixed period of time it asks all the actors to kill themselves.
 *
 * Note that the buffer actor can use two types of behavior:
 * one based on behavior change and the other based on state change.
 *
 * This class allows to a user to select the type of behavior,
 * the size of the buffer, the number of producers and consumers
 * and the length of the execution.
 *
 **/

public final class Initiator extends Behavior
{
    private static final long serialVersionUID = 1L;

    /**
     * Class constructor.
     *
     * the implementation flag: <code>true</code> for the behavior
     * change implementation" and <code>false</code> for the state
     *  change implementation.
     *
     *
     **/
    public Initiator()
    {
    }

    /** {@inheritDoc} **/
    @Override
    public void cases(final CaseFactory c) { }

    /**
     * Starts an actor space running the buffer example.
     *
     * @param v  the arguments.
     *
     * It does not need arguments.
     *
     **/
    public static void main(final String[] v)
    {
        Configuration conf =  SpaceInfo.INFO.getConfiguration();

        if(!conf.load(v)) {
            ErrorManager.raiseError(1, "Config file not found");
            return;
        }

        final String brokerIP = conf.getString("broker_ip");

        conf.setFilter(Logger.ACTIONS);
        conf.setLogFilter(new NoCycleProcessing());

        conf.addWriter(new ConsoleWriter());

        conf.setExecutor(new ThreadCoordinator());
        conf.setConnector(new ActiveMqConnector("tcp://" + brokerIP + ":61616"));
        conf.addService(new Creator());

        conf.start();
    }
}
